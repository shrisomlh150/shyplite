

//---------------------------------- First Part Of Assignment-----------------------/
function createArray(length) {
    let array = [...Array(length).keys()];

    console.log("Main ARRAY====>", array);

    return array;
}

let evenArray = async (array) => {

    let evenArray = [...array].filter(x => x % 2 == 0);

    console.log('Even Array=====>', evenArray);

}

function squareOfEven(key) {
    return new Promise((resolve, reject) => {
        if (!(key % 2)) {
            //even number
            resolve(key * key);
        } else {
            reject({
                message: `${key} is a odd number`
            })
        }
    })
}

let getSquareOfEven = async () => {

    let array = [2, 4, 10, 12, 13];

    let sum = 0;
    for (let key of array) {
        try {
            let squareResult = await squareOfEven(key);

            console.log("Square of Even====>", squareResult);

            sum += squareResult;
        } catch (ex) {
            console.log("Square of odd Result====>", ex);
        }
    }
    console.log("Sum Of Square======>", sum);
}

let questionFive = async (length) => {
    let array = [...Array(length).keys()];
    let errorCounter = 0;
    let sum = 0;
    let resultantArray = [];
    await array.map(async element => {
        try {
            let squareResult = await squareOfEven(element);
            sum += squareResult;
            resultantArray.push(squareResult);
        } catch (ex) {
            console.log("Square of odd Result====>", ex);
            errorCounter++;
        }
    })

    console.log("Number of Errors==============> ", errorCounter);
    console.log("The Resultant Array======>", resultantArray);
    console.log("Number of objects in the array=======>", resultantArray.length)
}

let array = createArray(101);
evenArray(array);
getSquareOfEven();
questionFive(101);

//----------------------END-------------------------------------------//

